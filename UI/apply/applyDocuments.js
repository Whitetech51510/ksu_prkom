'use strict';

var apply = angular.module('myApp.apply', ['ngRoute']);

apply.controller('ApplyDocumentCtrl', function($scope, userService, $location, infoService, applyService, directionService) {

    $scope.selectedPage = 'userDocuments';
    $scope.isLoading = true;
    $scope.userApplies = [];

    setTimeout(function () {
        getInformation();
    }, 500);

    function getInformation(){
        getUser();
        getUserApplies();
    }

    function getUser() {
        if (userService.User) {
            $scope.user = userService.User;
        } else {
            $location.path('/login');
        }
    }

    $scope.setPage = function () {
        return 'apply/includePages/' + $scope.selectedPage + '.html';
    }

    function getUserApplies(){
        setTimeout(function () {
            $scope.isLoading = false;
            $scope.userApplies.push("test");
                tryDigest()
        }, 400);
    }

    $scope.createApply = function(){
        directionService.getEducations().then(function(response){
            if(response.isSuccess){
                $scope.educations = response.object;
                $scope.selectedPage = 'applyPage';
            } else {
                infoService.infoFunction(response.message, "Ошибка");
            }
        });
    };

    function tryDigest() {
        if(!$scope.$$phase) {
            $scope.$apply();
        };
    }

});
