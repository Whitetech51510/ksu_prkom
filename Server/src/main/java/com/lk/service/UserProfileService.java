package com.lk.service;

import com.lk.entity.*;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/UserService")
@CrossOrigin(origins = "*", maxAge = 3600)
public interface UserProfileService {

    public Response getUserDocuments(HttpServletRequest request);

    public Response getUserEducation(HttpServletRequest request);

    public Response getUserRepresentative(HttpServletRequest request);

    public Response getUserAddress(HttpServletRequest request);

    public Response saveUserMainInfo(User user, HttpServletRequest request);

    public Response saveUserEducation(UserEducation user, HttpServletRequest request);

    public Response saveUserAddress(UserAddress user, HttpServletRequest request);

    public Response saveUserRepresentative(UserRepresentative user, HttpServletRequest request);

    public Response saveUserDocuments(UserDocument user, HttpServletRequest request);


}




