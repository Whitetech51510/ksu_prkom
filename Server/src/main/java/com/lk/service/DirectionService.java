package com.lk.service;

import com.lk.entity.Response;
import com.lk.entity.User;
import com.lk.entity.UserRegistration;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/DirectionService")
@CrossOrigin(origins = "*", maxAge = 3600)
public interface DirectionService {

    public Response getInstitutes();

    public Response getDirections();

    public Response getEducationForms();

    public Response getEducations();
}




