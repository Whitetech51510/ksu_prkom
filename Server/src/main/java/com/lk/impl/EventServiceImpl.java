package com.lk.impl;

import com.lk.entity.Event;
import com.lk.entity.Response;
import com.lk.persistence.HibernateUtil;
import com.lk.service.EventService;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.engine.transaction.spi.LocalStatus;

import javax.inject.Named;
import javax.transaction.Synchronization;
import java.sql.Timestamp;
import java.util.List;

@Named("eventService")
public class EventServiceImpl implements EventService {

    @Override
    public Response getEventByUserId(Integer UserId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Integer educationId = 1;
            Integer formEducationId = 1;
            Integer contesetGroupId = 1;
            Transaction transaction = null;

            transaction = session.beginTransaction();
            List<Event> events = session.createSQLQuery("SELECT * FROM abiturient_calendar_events " +
                    "WHERE education_id = :education_id AND form_education_id = :form_education_id AND contest_group_id = (:contest_group_id)")
                    .addEntity(Event.class)
                    .setParameter("education_id", educationId)
                    .setParameter("form_education_id", formEducationId)
                    .setParameter("form_education_id", contesetGroupId)
                    .list();
            transaction.commit();

            return new Response(true, events);
        }
        catch (Exception e) {
            return new Response(false, "Ошибка при получение событий: " + e.getLocalizedMessage());
        }
        finally {
            session.close();
        }
    }

    @Override
    public Response getAllEvents() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Transaction transaction = null;

            transaction = session.beginTransaction();
            List<Event> events = session.createSQLQuery(
                    "SELECT * FROM abiturient_calendar_events")
                    .addEntity(Event.class)
                    .list();
            transaction.commit();
            return new Response(true, events);
        }
        catch (Exception e) {
            return new Response(false, "Ошибка при получение событий: " + e.getLocalizedMessage());
        }
        finally {
            session.close();
        }
    }

    @Override
    public Response addEvent(Event event) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Transaction transaction = null;
            transaction = session.beginTransaction();
            session.createSQLQuery("INSERT INTO abiturient_calendar_events (event, date, education_id, form_education_id, contest_group_id)" +
                    "VALUES (((:event), (:date), (:education_id), (:form_education_id)), (:contest_group_id)")
                    .addEntity(Event.class)
                    .setParameter("event", event.getEvent())
                    .setParameter("date", event.getEventDate())
                    .setParameter("education_id", event.getEducationId())
                    .setParameter("form_education_id", event.getFormEducationId())
                    .setParameter("contest_group_id", event.getContestGroupId())
                    .executeUpdate();
            transaction.commit();
            return new Response(true, (Object)"Событие добавлено");
        }
        catch (Exception e) {
            return new Response(false, "Ошибка при получение событий: " + e.getLocalizedMessage());
        }
        finally {
            session.close();
        }
    }

    @Override
    public Response removeEvent(Integer eventId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try{
            Transaction transaction = null;

            transaction = session.beginTransaction();
            session.createSQLQuery("DELETE FROM abiturient_calendar_events " +
                    "WHERE id = :id")
                    .addEntity(Event.class)
                    .setParameter("id", eventId)
                    .executeUpdate();
            transaction.commit();

            return new Response(true);
        } catch (Exception e) {
            return new Response(false, "Ошибка при удалении события: " + e.getLocalizedMessage());
        } finally {
            session.close();
        }
    }
}
