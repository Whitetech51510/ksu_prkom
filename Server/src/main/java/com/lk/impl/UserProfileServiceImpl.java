package com.lk.impl;

import com.lk.entity.*;
import com.lk.persistence.HibernateUtil;
import com.lk.service.UserProfileService;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Named("userProfileService")
public class UserProfileServiceImpl implements UserProfileService {

    private static Logger logger = LoggerFactory.getLogger(UserProfileServiceImpl.class);

    @Override
    public Response getUserDocuments(HttpServletRequest request) {
        logger.info("Start function getUserDocuments");
        Session session = HibernateUtil.getSessionFactory().openSession();
        Integer user_id = 1;
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            List<UserDocument> userDocumentList = session.createSQLQuery("SELECT * FROM private_user_affair WHERE user_id = :thisID")
                    .addEntity(UserDocument.class)
                    .setParameter("thisID", user_id)
                    .list();
            transaction.commit();
            return new Response(true, userDocumentList.get(0));
        } catch (Exception ex){
            logger.error("Exception in getUserDocuments with:",ex.getLocalizedMessage(),ex);
            if (transaction!=null) { transaction.rollback(); }
            return new Response(false, "Ошибка получения документов: "+ex.toString());
        }
    }

    @Override
    public Response getUserEducation(HttpServletRequest request) {
        logger.info("Start function getUserEducation");
        Session session = HibernateUtil.getSessionFactory().openSession();
        Integer user_id = 1;
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            List<UserEducation> userEducationList = session.createSQLQuery("SELECT * FROM private_user_affair WHERE user_id = :thisID")
                    .addEntity(UserEducation.class)
                    .setParameter("thisID", user_id)
                    .list();
            transaction.commit();
            return new Response(true, userEducationList.get(0));
        } catch (Exception ex){
            logger.error("Exception in getUserEducation with:",ex.getLocalizedMessage(),ex);
            if (transaction!=null) { transaction.rollback(); }
            return new Response(false, "Ошибка получения образования: "+ex.toString());
        }
    }

    @Override
    public Response getUserRepresentative(HttpServletRequest request) {
        logger.info("Start function getUserRepresentative");
        Session session = HibernateUtil.getSessionFactory().openSession();
        Integer user_id = 1;
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            List<UserRepresentative> userRepresentativeList = session.createSQLQuery("SELECT * FROM private_user_affair WHERE user_id = :thisID")
                    .addEntity(UserRepresentative.class)
                    .setParameter("thisID", user_id)
                    .list();
            transaction.commit();
            return new Response(true, userRepresentativeList.get(0));
        } catch (Exception ex){
            logger.error("Exception in getUserRepresentative with:",ex.getLocalizedMessage(),ex);
            if (transaction!=null) { transaction.rollback(); }
            return new Response(false, "Ошибка получения представителя: "+ex.toString());
        }
    }

    @Override
    public Response getUserAddress(HttpServletRequest request) {
        logger.info("Start function getUserAddress");
        Session session = HibernateUtil.getSessionFactory().openSession();
        Integer user_id = 1;
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            List<UserAddress> userAddressList = session.createSQLQuery("SELECT * FROM private_user_affair WHERE user_id = :thisID")
                    .addEntity(UserAddress.class)
                    .setParameter("thisID", user_id)
                    .list();
            transaction.commit();
            return new Response(true, userAddressList.get(0));
        } catch (Exception ex){
            logger.error("Exception in getUserAddress with:",ex.getLocalizedMessage(),ex);
            if (transaction!=null) { transaction.rollback(); }
            return new Response(false, "Ошибка получения адреса: "+ex.toString());
        }
    }

    @Override
    public Response saveUserMainInfo(User user, HttpServletRequest request) {
        logger.info("Start function saveUserMainInfo");
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        Integer id = user.getId();
        String firstName = user.getFirstName();
        String lastName = user.getLastName();
        String patronymic = user.getPatronymic();
        String foreignLanguage = user.getForeignLanguage();
        String contactPhone = user.getContactPhone();
        Date dateOfBirth = user.getDateOfBirth();
        Boolean sex = user.getSex();
        try {
            transaction = session.beginTransaction();
            session.createSQLQuery("update users set fist_name = :firstName, last_name = :lastName, patronymic = :patronymic, foreign_language = :foreignLanguage," +
                    " contact_phone = :contactPhone, date_of_birth = :dateOfBirth, sex = :sex where id = :thisID")
                    .setParameter("firstName", firstName)
                    .setParameter("lastName", lastName)
                    .setParameter("patronymic", patronymic)
                    .setParameter("foreignLanguage", foreignLanguage)
                    .setParameter("contactPhone", contactPhone)
                    .setParameter("dateOfBirth", dateOfBirth)
                    .setParameter("sex", sex)
                    .setParameter("thisID", id)
                    .executeUpdate();
            transaction.commit();
            return new Response(true, (Object) "Данные успешно сохранены");
        } catch (Exception ex){
            if(transaction!=null) transaction.rollback();
            logger.error("Exception in saveUserInfo: " ,ex.getLocalizedMessage(),ex);
            return new Response(false, "Ошибка при сохранении: " + ex.getLocalizedMessage());
        } finally {
            session.close();
        }
    }

    @Override
    public Response saveUserEducation(UserEducation userEducation, HttpServletRequest request) {
        logger.info("Start function saveUserEducation");
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        Integer userId = userEducation.getUserId();
        Date receiveDate = userEducation.getReceiveDate();
        Date finishDate = userEducation.getFinishDate();
        String documentType = userEducation.getDocumentType();
        String series = userEducation.getSeries();
        String number = userEducation.getNumber();
        String country = userEducation.getCountry();
        String region = userEducation.getRegion();
        String district = userEducation.getDistrict();
        String receiveBy = userEducation.getReceiveBy();
        try {
            transaction = session.beginTransaction();
            session.createSQLQuery("update private_user_affair set usr_education_documenttype = :documentType, usr_education_series = :series, usr_education_number = :number, usr_education_receivedate = :receiveDate," +
                    " usr_education_receiveby = :receiveBy, usr_education_finishdate = :finishDate, usr_education_country = :country, usr_education_region = :region, usr_education_district = :district where user_id = :thisID")
                    .setParameter("documentType", documentType)
                    .setParameter("series", series)
                    .setParameter("number", number)
                    .setParameter("receiveDate", receiveDate)
                    .setParameter("receiveBy", receiveBy)
                    .setParameter("finishDate", finishDate)
                    .setParameter("country", country)
                    .setParameter("region", region)
                    .setParameter("district", district)
                    .setParameter("thisID", userId)
                    .executeUpdate();
            transaction.commit();
            return new Response(true, (Object) "Данные успешно сохранены");
        } catch (Exception ex){
            if(transaction!=null) transaction.rollback();
            logger.error("Exception in saveUserEducation: " ,ex.getLocalizedMessage(),ex);
            return new Response(false, "Ошибка при сохранении: " + ex.getLocalizedMessage());
        } finally {
            session.close();
        }
    }

    @Override
    public Response saveUserAddress(UserAddress userAddress, HttpServletRequest request) {
        logger.info("Start function saveUserAddress");
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        Integer userId = userAddress.getUserId();
        String country = userAddress.getCountry();
        String region = userAddress.getRegion();
        String localityType = userAddress.getLocalityType();
        String localityName = userAddress.getLocalityName();
        String street = userAddress.getStreet();
        String houseNumber = userAddress.getHouseNumber();
        String roomNumber = userAddress.getRoomNumber();
        try {
            transaction = session.beginTransaction();
            session.createSQLQuery("update private_user_affair set usr_address_country = :country, usr_address_region = :region, usr_address_locality_type = :localityType, usr_address_locality_name = :localityName," +
                    " usr_address_street = :street, usr_address_house_number = :houseNumber, usr_address_room_number = :roomNumber where user_id = :thisID")
                    .setParameter("country", country)
                    .setParameter("region", region)
                    .setParameter("localityType", localityType)
                    .setParameter("localityName", localityName)
                    .setParameter("street", street)
                    .setParameter("houseNumber", houseNumber)
                    .setParameter("roomNumber", roomNumber)
                    .setParameter("thisID", userId)
                    .executeUpdate();
            transaction.commit();
            return new Response(true, (Object) "Данные успешно сохранены");
        } catch (Exception ex){
            if(transaction!=null) transaction.rollback();
            logger.error("Exception in saveUserAddress: " ,ex.getLocalizedMessage(),ex);
            return new Response(false, "Ошибка при сохранении: " + ex.getLocalizedMessage());
        } finally {
            session.close();
        }
    }

    @Override
    public Response saveUserRepresentative(UserRepresentative userRepresentative, HttpServletRequest request) {
        logger.info("Start function saveUserRepresentative");
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        Integer userId = userRepresentative.getUserId();
        String lastName = userRepresentative.getLastName();
        String firstName = userRepresentative.getFirstName();
        String patronymic = userRepresentative.getPatronymic();
        String citizenship = userRepresentative.getCitizenship();
        String documentType = userRepresentative.getDocumentType();
        String series = userRepresentative.getSeries();
        String number = userRepresentative.getNumber();
        String documentCode = userRepresentative.getDocumentCode();
        Date receiveDate = userRepresentative.getReceiveDate();
        String receiveBy = userRepresentative.getReceiveBy();
        String placeOfBirth = userRepresentative.getPlaceOfBirth();
        try {
            transaction = session.beginTransaction();
            session.createSQLQuery("update private_user_affair set usr_representative_last_name = :lastName, usr_representative_first_name = :firstName, usr_representative_patronymic = :patronymic, usr_representative_citizenship = :citizenship, usr_representative_document_type = :documentType," +
                    " usr_representative_series = :series, usr_representative_number = :number, usr_representative_document_code = :documentCode, usr_representative_receivedate = :receiveDate, usr_representative_receiveby = :receiveBy, usr_representative_place_of_birth = :placeOfBirth where user_id = :thisID")
                    .setParameter("lastName", lastName)
                    .setParameter("firstName", firstName)
                    .setParameter("patronymic", patronymic)
                    .setParameter("citizenship", citizenship)
                    .setParameter("documentType", documentType)
                    .setParameter("series", series)
                    .setParameter("number", number)
                    .setParameter("documentCode", documentCode)
                    .setParameter("receiveDate", receiveDate)
                    .setParameter("receiveBy", receiveBy)
                    .setParameter("placeOfBirth", placeOfBirth)
                    .setParameter("thisID", userId)
                    .executeUpdate();
            transaction.commit();
            return new Response(true, (Object) "Данные успешно сохранены");
        } catch (Exception ex){
            if(transaction!=null) transaction.rollback();
            logger.error("Exception in saveUserRepresentative: " ,ex.getLocalizedMessage(),ex);
            return new Response(false, "Ошибка при сохранении: " + ex.getLocalizedMessage());
        } finally {
            session.close();
        }
    }

    @Override
    public Response saveUserDocuments(UserDocument userDocument, HttpServletRequest request) {
        logger.info("Start function saveUserDocuments");
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        Integer userId = userDocument.getUserId();
        String citizenship = userDocument.getCitizenship();
        String documentType = userDocument.getDocumentType();
        String series = userDocument.getSeries();
        String documentNumber = userDocument.getDocumentNumber();
        String documentCode = userDocument.getDocumentCode();
        Date   receiveDate = userDocument.getReceiveDate();
        String receiveBy = userDocument.getReceiveBy();
        String placeOfBirth = userDocument.getPlaceOfBirth();

        String oldCitizenship = userDocument.getOldCitizenship();
        String oldDocumentType = userDocument.getOldDocumentType();
        String oldSeries = userDocument.getOldSeries();
        String oldDocumentNumber = userDocument.getOldDocumentNumber();
        String oldDocumentCode = userDocument.getOldDocumentCode();
        Date oldReceiveDate = userDocument.getOldReceiveDate();
        String oldReceiveBy = userDocument.getOldReceiveBy();
        String oldPlaceOfBirth = userDocument.getOldPlaceOfBirth();
        try {
            transaction = session.beginTransaction();
            session.createSQLQuery("update private_user_affair set usr_document_citizenship = :citizenship, usr_document_type = :documentType, usr_document_series = :series, usr_document_number = :documentNumber," +
                    " usr_document_code = :documentCode, usr_document_receive_date = :receiveDate, usr_document_receive_by = :receiveBy, usr_document_place_of_birth = :placeOfBirth," +
                    " usr_old_document_citizenship = :oldCitizenship, usr_old_document_type = :oldDocumentType, usr_old_document_series = :oldSeries, usr_old_document_number = :oldDocumentNumber," +
                    " usr_old_document_code = :oldDocumentCode, usr_old_document_receive_date = :oldReceiveDate, usr_old_document_receive_by = :oldReceiveBy, usr_old_document_place_of_birth = :oldPlaceOfBirth where user_id = :thisID")
                    .setParameter("citizenship", citizenship)
                    .setParameter("documentType", documentType)
                    .setParameter("series", series)
                    .setParameter("documentNumber", documentNumber)
                    .setParameter("documentCode", documentCode)
                    .setParameter("receiveDate", receiveDate)
                    .setParameter("receiveBy", receiveBy)
                    .setParameter("placeOfBirth", placeOfBirth)
                    .setParameter("oldCitizenship", oldCitizenship)
                    .setParameter("oldDocumentType", oldDocumentType)
                    .setParameter("oldSeries", oldSeries)
                    .setParameter("oldDocumentNumber",oldDocumentNumber)
                    .setParameter("oldDocumentCode", oldDocumentCode)
                    .setParameter("oldReceiveDate", oldReceiveDate)
                    .setParameter("oldReceiveBy", oldReceiveBy)
                    .setParameter("oldPlaceOfBirth", oldPlaceOfBirth)
                    .setParameter("thisID", userId)
                    .executeUpdate();
            transaction.commit();
            return new Response(true, (Object) "Данные успешно сохранены");
        } catch (Exception ex){
            if(transaction!=null) transaction.rollback();
            logger.error("Exception in saveUserDocuments: " ,ex.getLocalizedMessage(),ex);
            return new Response(false, "Ошибка при сохранении: " + ex.getLocalizedMessage());
        } finally {
            session.close();
        }
    }


}
