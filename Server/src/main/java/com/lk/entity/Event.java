package com.lk.entity;

import java.sql.Timestamp;

public class Event {
    private Integer Id;
    private Integer EducationId;
    private Integer FormEducationId;
    private String Event;
    private Timestamp EventDate;
    private Integer ContestGroupId;

    public Event(){ }

    public Timestamp getEventDate() { return EventDate; }
    public void setEventDate(Timestamp eventDate) { EventDate = eventDate; }
    public String getEvent() { return Event; }
    public void setEvent(String event) { Event = event; }
    public Integer getId() { return Id; }
    public void setId(Integer id) { Id = id; }
    public Integer getEducationId() { return EducationId; }
    public void setEducationId(Integer educationId) { EducationId = educationId; }
    public Integer getFormEducationId() { return FormEducationId; }
    public void setFormEducationId(Integer formEducationId) { FormEducationId = formEducationId; }
    public Integer getContestGroupId() { return ContestGroupId; }
    public void setContestGroupId(Integer contestGroupId) { ContestGroupId = contestGroupId; }
}
