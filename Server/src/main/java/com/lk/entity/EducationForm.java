package com.lk.entity;

public class EducationForm {
    private Integer Id;
    private String EducationForm;
    public Integer getId() { return Id; }
    public void setId(Integer id) { Id = id; }
    public String getEducationForm() { return EducationForm; }
    public void setEducationForm(String Form) { EducationForm = Form; }
}
