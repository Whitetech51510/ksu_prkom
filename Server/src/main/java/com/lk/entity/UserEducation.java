package com.lk.entity;

import java.util.Date;

public class UserEducation {

    private Integer UserId;
    private Date ReceiveDate;
    private Date FinishDate;
    private String DocumentType;
    private String Series;
    private String Number;
    private String Country;
    private String Region;
    private String District;
    private String ReceiveBy;

    public UserEducation() { }

    public Integer getUserId() { return UserId; }
    public void setUserId(Integer userId) { UserId = userId; }
    public Date getReceiveDate() { return ReceiveDate; }
    public void setReceiveDate(Date receiveDate) { ReceiveDate = receiveDate; }
    public Date getFinishDate() { return FinishDate; }
    public void setFinishDate(Date finishDate) { FinishDate = finishDate; }
    public String getDocumentType() { return DocumentType; }
    public void setDocumentType(String documentType) { DocumentType = documentType; }
    public String getSeries() { return Series; }
    public void setSeries(String series) { Series = series; }
    public String getNumber() { return Number; }
    public void setNumber(String number) { Number = number; }
    public String getCountry() { return Country; }
    public void setCountry(String country) { Country = country; }
    public String getRegion() { return Region; }
    public void setRegion(String region) { Region = region; }
    public String getDistrict() { return District; }
    public void setDistrict(String district) { District = district; }
    public String getReceiveBy() { return ReceiveBy; }
    public void setReceiveBy(String receiveBy) { ReceiveBy = receiveBy; }

}
