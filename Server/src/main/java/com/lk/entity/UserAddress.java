package com.lk.entity;


public class UserAddress {

    private Integer UserId;
    private String Country;
    private String Region;
    private String LocalityType;
    private String LocalityName;
    private String Street;
    private String HouseNumber;
    private String RoomNumber;

    public UserAddress(){}

    public Integer getUserId() { return UserId; }
    public void setUserId(Integer userID) { UserId = userID; }
    public String getCountry() { return Country; }
    public void setCountry(String country) { Country = country; }
    public String getRegion() { return Region; }
    public void setRegion(String region) { Region = region; }
    public String getLocalityType() { return LocalityType; }
    public void setLocalityType(String localityType) { LocalityType = localityType; }
    public String getLocalityName() { return LocalityName; }
    public void setLocalityName(String localityName) { LocalityName = localityName; }
    public String getStreet() { return Street; }
    public void setStreet(String street) { Street = street; }
    public String getHouseNumber() { return HouseNumber; }
    public void setHouseNumber(String houseNumber) { HouseNumber = houseNumber; }
    public String getRoomNumber() { return RoomNumber; }
    public void setRoomNumber(String roomNumber) { RoomNumber = roomNumber; }

}
