package com.lk.entity;

import java.util.Date;

public class UserRepresentative {

    private Integer UserId;
    private String FirstName;
    private String LastName;
    private String Patronymic;
    private String Citizenship;
    private String DocumentType;
    private String Series;
    private String Number;
    private String DocumentCode;
    private String PlaceOfBirth;
    private String ReceiveBy;
    private Date ReceiveDate ;


    public UserRepresentative(){}

    public Integer getUserId() { return UserId; }
    public void setUserId(Integer id) { UserId = id; }
    public String getFirstName() { return FirstName; }
    public void setFirstName(String firstName) { FirstName = firstName; }
    public String getLastName() { return LastName; }
    public void setLastName(String lastName) { LastName = lastName; }
    public String getPatronymic() { return Patronymic; }
    public void setPatronymic(String patronymic) { Patronymic = patronymic; }
    public String getCitizenship() { return Citizenship; }
    public void setCitizenship(String citizenship) { Citizenship = citizenship; }
    public String getDocumentType() { return DocumentType; }
    public void setDocumentType(String documentType) { DocumentType = documentType; }
    public String getSeries() { return Series; }
    public void setSeries(String series) { Series = series; }
    public String getNumber() { return Number; }
    public void setNumber(String number) { Number = number; }
    public String getDocumentCode() { return DocumentCode; }
    public void setDocumentCode(String documentCode) { DocumentCode = documentCode; }
    public String getPlaceOfBirth() { return PlaceOfBirth; }
    public void setPlaceOfBirth(String placeOfBirth) { PlaceOfBirth = placeOfBirth; }
    public String getReceiveBy() { return ReceiveBy; }
    public void setReceiveBy(String receiveBy) { ReceiveBy = receiveBy; }
    public Date getReceiveDate() { return ReceiveDate; }
    public void setReceiveDate(Date receiveDate) { ReceiveDate = receiveDate; }
}
