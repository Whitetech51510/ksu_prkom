package com.lk.controller;

import com.lk.entity.Event;
import com.lk.entity.Response;
import com.lk.service.EventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.inject.Named;

@RestController
@RequestMapping("/EventService")
@CrossOrigin(origins = "*", maxAge = 3600)
public class EventServiceRestController {

    private EventService eventService;
    private Logger logger = LoggerFactory.getLogger(EventServiceRestController.class);

    @Inject
    public EventServiceRestController(@Named("eventService") EventService eventService) {
        this.eventService = eventService;
    }

    @RequestMapping(value = "/getAllEvents", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Response getAllEvents () { return eventService.getAllEvents(); }

    @RequestMapping(value = "/getEventByUserId/{userId}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Response getEventByUserId (@PathVariable("userId") Integer userId){
        return eventService.getEventByUserId(userId);
    }

    @RequestMapping(value = "/addEvent", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public Response addEvent (@RequestBody Event event) {
        return eventService.addEvent(event);
    }

    @RequestMapping(value = "/removeEvent/{eventId}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public Response removeEvent (@PathVariable("eventId") Integer eventId) {
        return eventService.removeEvent(eventId);
    }
}
