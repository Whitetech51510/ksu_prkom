package com.lk.controller;

import com.lk.entity.Response;
import com.lk.service.DirectionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.inject.Named;

@RestController
@RequestMapping("/DirectionService")
@CrossOrigin(origins = "*", maxAge = 3600)
public class DirectionServiceRestController {

 private DirectionService directionService;
    private Logger logger = LoggerFactory.getLogger(DirectionServiceRestController.class);

    @Inject
    public DirectionServiceRestController(@Named("directionService") DirectionService directionService) {
        this.directionService = directionService;
    }

    @RequestMapping(value = "/getInstitutes", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Response getInstitutions () {
        return directionService.getInstitutes();
    }

    @RequestMapping(value = "/getDirections", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Response getDirections () {
        return directionService.getDirections();
    }

    @RequestMapping(value = "/getEducationForms", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Response getEducationForms () {
        return directionService.getEducationForms();
    }

    @RequestMapping(value = "/getEducations", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Response getEducations () {
        return directionService.getEducations();
    }

}
